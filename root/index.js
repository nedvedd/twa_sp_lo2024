let currentBalance = 0;
let monthIncome = 0;
let monthExpenses = 0;
let monthBalance = 0;

let categoryTableBody = document.getElementById("categoryTableBody");
let categoriesArray = [];

let czk = Intl.NumberFormat('cs-CZ', {
    style: 'currency',
    currency: 'CZK',
});

/**
 * Nastavi do navbaru aktualni datum
 */
function setCurrentDate() {
    let currentDate = new Date().toLocaleDateString('cs-CZ', { weekday:"long", year:"numeric", month:"short", day:"numeric"})
    document.querySelector("#currentDate").innerHTML = currentDate;
}

/**
 * Upravi aktualni zustatek
 */
function editBalance() {
    currentBalance = document.forms["editBalanceForm"]["newBalance"].value;
    localStorage.setItem("currentBalance", currentBalance)

    document.querySelector("#balance").innerHTML = czk.format(currentBalance);
    console.log("Balance edited.");
    let modal = bootstrap.Modal.getInstance(document.querySelector("#editBalanceModal"))
    modal.hide();
}

/**
 * Upravi mesicni prijmy, vydaje a zustatek
 */
function editMonthBalance() {
    monthIncome = document.forms["editMonthBalanceModalForm"]["newIncome"].value;
    monthExpenses = document.forms["editMonthBalanceModalForm"]["newExpenses"].value;
    monthBalance = monthIncome - monthExpenses;

    localStorage.setItem("monthIncome", monthIncome)
    localStorage.setItem("monthExpenses", monthExpenses)
    localStorage.setItem("monthBalance", monthBalance)

    updateValues();

    console.log("Month balance edited.");
    let modal = bootstrap.Modal.getInstance(document.querySelector("#editMonthBalanceModal"))
    modal.hide();
}

/**
 * Vytvori novou kategorii
 */
function createNewCategory() {
    let newCategoryName = document.forms["newCategoryForm"]["categoryName"].value;
    let newCategoryExpectedExpenses = document.forms["newCategoryForm"]["categoryExpected"].value;
    let newCategoryCurrentExpenses = document.forms["newCategoryForm"]["categoryAlreadySpent"].value;

    let newCategory = new Category(newCategoryName, newCategoryExpectedExpenses, newCategoryCurrentExpenses);
    if (categoriesArray != null) {
        categoriesArray.push(newCategory);
    } else {
        categoriesArray = [newCategory];
    }


    localStorage.setItem("categoriesArray", JSON.stringify(categoriesArray));
    location.reload();

    updatePieChartParams();

    console.log("New category created.")
}

let currentRow = 0;

/**
 * Pomocna metoda pro urceni konreteniho radku, u ktereho je vyvolana editace
 */
function setCurrentRow() {
    let table = document.querySelector("table");

    for (let i = 0; i < table.rows.length; i++) {
        table.rows.item(i).addEventListener("click", e => {
            currentRow = i - 1;
            console.log("Current row is on index: " + currentRow);

        })
    }
}

/**
 * Smaze kategorii
 */
function deleteCategory() {
    let table = document.querySelector("table");
    table.deleteRow((currentRow + 1));
    console.log(currentRow + ". row deleted.")

    categoriesArray.splice(currentRow, currentRow);
    localStorage.setItem("categoriesArray", JSON.stringify(categoriesArray));
    location.reload();
}

/**
 * Upravi kategorii
 */
function editCategory() {
    console.log("Edit category triggered.")
}

/**
 * Aktualizuje tabulku
 */
function updateTable() {

    if (categoriesArray === null) {
        categoriesArray = [];
    }

    // categoryTableBody.innerHTML
    for (let i = 0; i < categoriesArray.length; i++) {
        let newRow = document.createElement("tr");

        let categoryNameCell = document.createElement("td");
        let categoryExpectedExpensesCell = document.createElement("td");
        let categoryCurrentExpensesNameCell = document.createElement("td");


        let buttonsDiv= document.createElement("div");
        buttonsDiv.innerHTML +=
            `<button type="button" class="btn btn-secondary editBtn" data-bs-toggle="modal" data-bs-target="#editCategoryModal"">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325"/>
                </svg>
            </button>`;


        let buttonsCell = document.createElement("td");
        buttonsCell.appendChild(buttonsDiv);

        newRow.appendChild(categoryNameCell);
        newRow.appendChild(categoryExpectedExpensesCell);
        newRow.appendChild(categoryCurrentExpensesNameCell);
        newRow.appendChild(buttonsCell);

        categoryTableBody.appendChild(newRow);
        categoryTableBody.classList.add("table");

        categoryNameCell.innerHTML = categoriesArray[i].name;
        categoryExpectedExpensesCell.innerHTML = czk.format(categoriesArray[i].expectedExpenses);
        categoryCurrentExpensesNameCell.innerHTML = czk.format(categoriesArray[i].currentExpenses) + " (" + (categoriesArray[i].currentExpenses/categoriesArray[i].expectedExpenses * 100) + " %)";
    }

    let btnRow = document.createElement("tr");
    btnRow.innerHTML +=
        `<td colSpan="3" id="newCategoryTd">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary plusBtn" data-bs-toggle="modal" data-bs-target="#newCategoryModal">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2"/>
                </svg>
            </button>
        </td>`;
    categoryTableBody.appendChild(btnRow);
}

/**
 * Nacte nutna data pro zobrazeni stranky (grafy)
 * a pripadne nacte ulozena data z localStorage
 */
function initializeData() {
    currentBalance = localStorage.getItem("currentBalance");
    monthIncome = localStorage.getItem("monthIncome");
    monthExpenses = localStorage.getItem("monthExpenses");
    monthBalance = localStorage.getItem("monthBalance");

    categoriesArray = JSON.parse(localStorage.getItem("categoriesArray"));

    updateValues();

    updateTable();

    console.log("Values loaded from local storage.");

    updatePieChartParams();
    getPieChart();
    getColumnChart();

    switchTheme(true);
    setCurrentRow();
}

/**
 * Aktualizuje hodnoty panelu s celkovym a mesicnim zustatkem
 */
function updateValues() {
    document.querySelector("#balance").innerHTML = czk.format(currentBalance);

    document.querySelector("#income").innerHTML = czk.format(monthIncome);
    document.querySelector("#expenses").innerHTML = czk.format(monthExpenses);
    document.querySelector("#monthBalance").innerHTML = czk.format(monthBalance);

    if (monthBalance < 0) {
        document.querySelector("#monthBalance").style = "color: #b0000f";
    } else {
        document.querySelector("#monthBalance").style = "color: #00E396;";
    }
}

/**
 * Trida reprezentujici kategorii vydaju
 */
class Category {
    constructor(name, expectedExpenses, currentExpenses) {
        this.name = name;
        this.expectedExpenses = expectedExpenses;
        this.currentExpenses = currentExpenses;
    }
}

//==================================== Charts ====================================

let pieChart;
let columnChart;
let pieChartParams = {
    "categories": [],
    "categoriesValues": []
}

/**
 * Aktualizuje statistiku ke kolacovemu grafu s prehledem mesicnich vydaju
 */
async function updatePieChartParams() {
    for (let i = 0; i < categoriesArray.length; i++) {
        pieChartParams.categories.push(categoriesArray[i].name);
        pieChartParams.categoriesValues.push(Number(categoriesArray[i].currentExpenses));
    }
    console.log(pieChartParams)
    await pieChart.updateOptions({
        labels: pieChartParams.categories,
        series: pieChartParams.categoriesValues
    });
    await pieChart.render();
}

/**
 * Vygeneruje kolacovy graf s prehledem mesicnich vydaju
 */
async function getPieChart() {
    let pieChartOptions = {
        chart: {
            foreColor: 'white',
            type: 'donut',
            width: '100%'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '10%'
                }
            }
        },
        legend: {
            position: 'bottom',
        },

        labels: pieChartParams.categories,
        series: pieChartParams.categoriesValues,

    }

    pieChart = new ApexCharts(document.querySelector("#pieChart"), pieChartOptions);
    await pieChart.render();
}

/**
 * Vygeneruje graf s porovnanim prijmu vs. vydaju za uplynule mesice
 */
async function getColumnChart() {
    let columnChartOptions = {
        series: [
            {
                name: 'Příjmy',
                data: [
                    {
                        x: 'listopad',
                        y: 30000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 18000,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'prosinec',
                        y: 29000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 18500,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'leden',
                        y: 28000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 19000,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'únor',
                        y: 28000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 17000,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'březen',
                        y: 29500,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 20000,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'duben',
                        y: 31000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 18000,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'květen',
                        y: 28000,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: 19000,
                                strokeHeight: 5,
                                strokeDashArray: 5,
                                strokeColor: '#b0000f'
                            }
                        ]
                    },
                    {
                        x: 'červen',
                        y: monthIncome,
                        goals: [
                            {
                                name: 'Výdaje',
                                value: monthExpenses,
                                strokeHeight: 5,
                                strokeColor: '#b0000f'

                            }
                        ]
                    }
                ]
            }
        ],
        chart: {
            height: 350,
            type: 'bar',
            foreColor: 'white',
        },
        plotOptions: {
            bar: {
                columnWidth: '60%'
            }
        },
        colors: ['#00E396'],
        dataLabels: {
            enabled: false
        },
        legend: {
            show: true,
            showForSingleSeries: true,
            customLegendItems: ['Příjmy', 'Výdaje'],
            markers: {
                fillColors: ['#00E396', '#b0000f']
            }
        },
        tooltip: {
            theme: "dark"
        }
    };

    columnChart = new ApexCharts(document.querySelector("#columnChart"), columnChartOptions);
    await columnChart.render();

}

/**
 * Prepne styl na svetly/tmavy
 *
 * @param pageLoad  true, pokud je funkce vyvolana pri nacteni stranky (zjisti, zda pri posledni relaci byl nastaveny styl a nastavi ho)
 *                  false, pokud je funkce vyvolana skrz tlacitko
 */
function switchTheme(pageLoad) {
    if (pageLoad === true) {
        let theme = localStorage.getItem("theme");
        if (theme === "dark") {
            setDarkTheme();
        } else {
            setLightTheme();
        }
    }
    else if (document.documentElement.getAttribute('data-bs-theme') === 'dark') {
        setLightTheme();
    }
    else {
        setDarkTheme();
    }
}

/**
 * Nastavi styl na svetly
 */
async function setLightTheme() {
    document.documentElement.setAttribute('data-bs-theme', 'light');
    document.querySelector("#switchThemeBtn").innerHTML =
        `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-brightness-high" viewBox="0 0 16 16">
            <path d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6m0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8M8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0m0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13m8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5M3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8m10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0m-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0m9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707M4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708"/>
        </svg>`;

    await pieChart.updateOptions({
        chart: {
            foreColor: 'black',
        }
    });

    await columnChart.updateOptions({
        chart: {
            foreColor: 'black',
        },
        tooltip: {
            theme: "light"
        }
    });

    localStorage.setItem("theme", "light");
}

/**
 * Nastavi styl na tmavy
 */
async function setDarkTheme() {
    document.documentElement.setAttribute('data-bs-theme', 'dark');
    document.querySelector("#switchThemeBtn").innerHTML =
        `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-moon" viewBox="0 0 16 16">
            <path d="M6 .278a.77.77 0 0 1 .08.858 7.2 7.2 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277q.792-.001 1.533-.16a.79.79 0 0 1 .81.316.73.73 0 0 1-.031.893A8.35 8.35 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.75.75 0 0 1 6 .278M4.858 1.311A7.27 7.27 0 0 0 1.025 7.71c0 4.02 3.279 7.276 7.319 7.276a7.32 7.32 0 0 0 5.205-2.162q-.506.063-1.029.063c-4.61 0-8.343-3.714-8.343-8.29 0-1.167.242-2.278.681-3.286"/>
        </svg>`;

    await pieChart.updateOptions({
        chart: {
            foreColor: 'white',
        }
    });

    await columnChart.updateOptions({
        chart: {
            foreColor: 'white',
        },
        tooltip: {
            theme: "dark"
        }
    });

    localStorage.setItem("theme", "dark");
}
